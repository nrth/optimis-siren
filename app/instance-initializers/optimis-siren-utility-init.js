import init from 'optimis-siren/instance-initializers/optimis-siren-utility-init';

export function initialize(applicationInstance) {
    init(applicationInstance);
}

export default {
    name: 'optimis-siren-utility-init',
    initialize: initialize,
    after: ['optimis-siren-service-init']
};
