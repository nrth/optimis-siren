import ENV from '../config/environment';
import Authentication from '../authentication';
import Authorization from '../authorization';
import init from 'optimis-siren/instance-initializers/optimis-siren-service-init';

export function initialize(applicationInstance) {
    init(applicationInstance, ENV, Authentication, Authorization);
}

export default {
    name: 'optimis-siren-service-init',
    initialize: initialize
};
