import ENV from '../config/environment';
import config from 'optimis-siren/config';
import init from 'optimis-siren/initializers/optimis-siren';

export function initialize(application) {
    config.load(ENV);
    init(application);
}

export default {
    name: 'optimis-siren',
    after: ['ember-data', 'ember-simple-auth'],
    initialize
};
