/* eslint-env node */
'use strict';

const mergeTrees = require('broccoli-merge-trees');
const funnel = require('broccoli-funnel');
const path = require('path');

module.exports = {
    name: 'optimis-siren',

    isDevelopingAddon() {
        return true;
    },

    included(app) {
        this._super.included.apply(this, arguments);

        this.app = app;

        this.importBrowserDependencies(app);
    },

    importBrowserDependencies(app) {
        let vendor = this.treePaths.vendor;

        // forge
        app.import(`${vendor}/forge/forge.min.js`);
        // node-uuid
        app.import(`${vendor}/node-uuid/uuid.js`);
        // enquire.js
        app.import({
            development: `${vendor}/enquire.js/enquire.js`,
            production: `${vendor}/enquire.js/enquire.min.js`
        });
        app.import(`${vendor}/shims/enquire.js`);
    },

    treeForVendor(vendorTree) {
        return this.treeForBrowserVendor(vendorTree);
    },

    treeForBrowserVendor(vendorTree) {
        let trees = [];
        let nodeUuidJsPath = path.dirname(require.resolve('node-uuid'));
        let enquireJsPath = path.join(path.dirname(require.resolve('enquire.js')), '..', 'dist');

        if (vendorTree) {
            trees.push(vendorTree);
        }

        trees.push(
            funnel(nodeUuidJsPath, {
                destDir: 'node-uuid',
                include: ['uuid.js']
            })
        );

        trees.push(
            funnel(enquireJsPath, {
                destDir: 'enquire.js',
                include: ['*.js']
            })
        );

        return mergeTrees(trees);
    }
};
