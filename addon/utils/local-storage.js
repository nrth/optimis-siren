import _ from 'lodash';

const { isNull } = _;

export default {
	set(contextName, key, value) {
		let json = JSON.stringify(value);

		localStorage.setItem(this._getKey(contextName, key), json);
	},

	get(contextName, key, defaultValue) {
		let json = localStorage.getItem(this._getKey(contextName, key));

		if (isNull(json)) {
			return defaultValue;
		}

		let value = JSON.parse(json);

		return value;
	},

    _getKey(contextName, key) {
        return `[${contextName}]::${key}`;
    }
};
