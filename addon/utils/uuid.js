/* globals uuid */
import moment from 'moment';
import config from '../config';

const UtcUnixStart = moment.utc('1970-01-01');
const _uuid = {
    generate(date) {
        let bytes = new Uint8Array(16);
        let now = date ? moment.utc(date) : moment.utc();
        let days = now.diff(UtcUnixStart, 'days');
        let time = now.diff(now.clone().startOf('day'), 'milliseconds');
        let dayBytes = this._getBigEndianBytes(days);
        let timeBytes = this._getBigEndianBytes(time);

        uuid.v4(null, bytes, 0);

        if (config.isGuidByteArray) {
            bytes[0] = timeBytes[1];
            bytes[1] = timeBytes[0];
            bytes[2] = dayBytes[3];
            bytes[3] = dayBytes[2];
            bytes[4] = timeBytes[3];
            bytes[5] = timeBytes[2];
        } else {
            bytes[10] = dayBytes[2];
            bytes[11] = dayBytes[3];
            bytes[12] = timeBytes[0];
            bytes[13] = timeBytes[1];
            bytes[14] = timeBytes[2];
            bytes[15] = timeBytes[3];
        }

        let result = uuid.unparse(bytes);

        return result;
    },

    fromNumber(number) {
        number = number | 0;

        let bytes = new Uint8Array(16);
        let numberBytes = this._getBigEndianBytes(number);

        // 0x40
        bytes[6] = 64;
        // 0xa0
        bytes[8] = 160;

        if (config.isGuidByteArray) {
            bytes[0] = numberBytes[3];
            bytes[1] = numberBytes[2];
            bytes[2] = numberBytes[1];
            bytes[3] = numberBytes[0];
        } else {
            bytes[10] = numberBytes[0];
            bytes[11] = numberBytes[1];
            bytes[12] = numberBytes[2];
            bytes[13] = numberBytes[3];
        }

        let result = uuid.unparse(bytes);

        return result;
    },

    toMoment(_uuid) {
        let bytes = uuid.parse(_uuid);
        let daysBytes = config.isGuidByteArray ? [0, 0, bytes[3], bytes[2]] : [0, 0, bytes[10], bytes[11]];
        let timeBytes = config.isGuidByteArray ? [bytes[1], bytes[0], bytes[5], bytes[4]] : [bytes[12], bytes[13], bytes[14], bytes[15]];
        let days = this._getNumberFromBigEndianBytes(daysBytes);
        let time = this._getNumberFromBigEndianBytes(timeBytes);
        let result = moment(UtcUnixStart);

        result.add(days, 'day');
        result.add(time, 'millisecond');

        return result;
    },

    _getBigEndianBytes(number) {
        number = number | 0;

        return new Uint8Array([
            (number & 0xFF000000) >> 24,
            (number & 0x00FF0000) >> 16,
            (number & 0x0000FF00) >> 8,
            (number & 0x000000FF)
        ]);
    },

    _getNumberFromBigEndianBytes(bytes) {
        return (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3];
    }
};

export default { uuid: _uuid };
