export default {
    getSessionDataFromToken(token) {
        let base64Payload = token.split('.')[1];
        let payloadJson = atob(base64Payload);
        let payload = JSON.parse(payloadJson);

        return {
            userAccountId: `${payload.sub}`,
            token: token
        };
    }
};
