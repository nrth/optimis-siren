import _ from 'lodash';
import config from '../config';

const { isString, isPlainObject, trimEnd, trim, reduce, join, isUndefined } = _;

export default {
	build(endpoint, queryParams) {
        let host = trimEnd(config.host, '/');
        let rootUrl = trim(config.rootUrl, '/');
        let apiNamespace = trim(config.apiNamespace, '/');
		let url = isString(host) && host.length > 0 ? `${host}/` : '/';
		
		if (isString(rootUrl) && rootUrl.length > 0) {
			url += `${rootUrl}/`;
		}
		
		if (isString(apiNamespace) && apiNamespace.length > 0) {
			url += `${apiNamespace}/`;
		}
		
		if (isString(endpoint)) {
			url += trim(endpoint, '/');
		}
		
		if (isPlainObject(queryParams)) {
			let queryStringComponents = reduce(queryParams, (result, value, key) => {
				if (!isUndefined(value)) {
					result.pushObject(`${key}=${encodeURIComponent(value)}`);
				}
				
				return result;
			}, []);
			
			if (queryStringComponents.length > 0) {
				url += `?${join(queryStringComponents, '&')}`;
			}
		}
		
		return url;
	}
};
