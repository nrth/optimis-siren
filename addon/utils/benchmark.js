import _ from 'lodash';
import RSVP from 'rsvp';

const { isString, isPlainObject, isFunction } = _;
const { Promise } = RSVP;

export default {
    logger: null,

    async(fn, msg) {
        let start = window.performance.now();

        return Promise.resolve(fn()).then(result => {
            let end = window.performance.now();
            let timeTaken = Math.round((end - start) * 1000) / 1000;
            let message = this._buildMessage(msg, timeTaken);

            this.logger.debug(message);

            return result;
        });
    },

    sync(fn, msg) {
        let start = window.performance.now();
        let result = fn();
        let end = window.performance.now();
        let timeTaken = Math.round((end - start) * 1000) / 1000;
        let message = this._buildMessage(msg, timeTaken);

        this.logger.debug(message);

        return result;
    },

    _buildMessage(msg, ms) {
        if (isFunction(msg)) {
            return msg(ms);
        } else if (isPlainObject(msg)) {
            let hasType = isString(msg.type) && msg.type.length > 0;
            let hasName = isString(msg.name) && msg.name.length > 0;
            let hasMethod = isString(msg.method) && msg.method.length > 0;

            if (!hasType || !hasName || !hasMethod) {
                throw new Error(`'type', 'name', and 'method' should be specified for benchmarking`);
            }

            let message = isFunction(msg.messageFn) ? msg.messageFn(ms) : `${ms} ms`;

            return `[${msg.type}:${msg.name}.${msg.method}] ${message}`;
        }

        throw new Error(`'msg' should be a function or Object`);
    }
};
