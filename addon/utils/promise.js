import Ember from 'ember';
import _ from 'lodash';
import RSVP from 'rsvp';

const { run } = Ember;
const { later, cancel } = run;
const { isUndefined } = _;
const { Promise } = RSVP;

export default {
    create(fn, timeout) {
        timeout = _.parseInt(timeout);

        if (isNaN(timeout) || timeout < 0) {
            timeout = undefined;
        }

        return new Promise((resolve, reject) => {
            let context = { isDone: false };

            if (timeout) {
                context.timer = later(null, () => {
                    if (!context.isDone) {
                        context.isDone = true;

                        run(null, reject, new Error('Promise timed out'));
                    }
                }, timeout);
            }
            
            function _resolve() {
                if (context.timer) {
                    cancel(context.timer);
                }

                if (context.isDone) {
                    return;
                }

                context.isDone = true;

                resolve.apply(this, arguments);
            }

            function _reject() {
                if (context.timer) {
                    cancel(context.timer);
                }

                if (context.isDone) {
                    return;
                }

                context.isDone = true;

                reject.apply(this, arguments);
            }

            try {
                let result = fn(_resolve, _reject);

                if (!isUndefined(result)) {
                    Promise.resolve(result).then(value => {
                        run(null, _resolve, value);
                    }).catch(error => {
                        run(null, _reject, error);
                    });
                }
            } catch (error) {
                run(null, _reject, error);
            }
        });
    }
};
