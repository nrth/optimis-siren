import Ember from 'ember';
import BaseAuthorizer from 'ember-simple-auth/authorizers/base';
import _ from 'lodash';

const { get, isPresent } = Ember;
const { isString } = _;

export default BaseAuthorizer.extend({
    authorize(data, block) {
        let token = get(data, 'token');
        let hasToken = isString(token) && isPresent(token);

        if (hasToken) {
            block('Authorization', `Bearer ${token}`);
        }
    }
});
