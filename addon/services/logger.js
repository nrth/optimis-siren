import Ember from 'ember';
import config from 'optimis-siren/config';

const { Service, Logger } = Ember;
const { logLevel } = config;

export default Service.extend({
    canDebug: logLevel === 'debug',
    canInfo: logLevel === 'debug' || logLevel === 'info',
    canWarn: logLevel === 'debug' || logLevel === 'info' || logLevel === 'warn',
    canError: logLevel === 'debug' || logLevel === 'info' || logLevel === 'warn' || logLevel === 'error',
    canLog: logLevel === 'debug' || logLevel === 'info' || logLevel === 'warn' || logLevel === 'error' || logLevel === 'log',

    log() {
        if (this.get('canLog')) {
            Logger.log(...arguments);
        }
    },

    debug() {
        if (this.get('canDebug')) {
            Logger.debug(...arguments);
        }
    },

    info() {
        if (this.get('canInfo')) {
            Logger.info(...arguments);
        }
    },

    warn() {
        if (this.get('canWarn')) {
            Logger.warn(...arguments);
        }
    },

    error() {
        if (this.get('canError')) {
            Logger.error(...arguments);
        }
    }
});
