import Ember from 'ember';
import fetch from 'fetch';
import BuildUrlMixin from '../mixins/build-url-mixin';
import ModelNameMixin from '../mixins/model-name-mixin';
import _ from 'lodash';
import RSVP from 'rsvp';

const { Service, isNone, isPresent, get, set, computed, run } = Ember;
const { readOnly, alias } = computed;
const { isString } = _;
const { Promise } = RSVP;

export default Service.extend(BuildUrlMixin, ModelNameMixin, {
    authToken: readOnly('session.data.authenticated.token'),
    distributedSession: alias('session.data.distributedSession'),

    usingSession(promiseFn) {
        try {
            return this.post('session/create')
                .then(([sessionId]) => {
                    this.set('distributedSession', sessionId);

                    return promiseFn()
                        .then(() => this.post(`session/${sessionId}/commit`, null, null, true))
                        .then(response => {
                            this.set('distributedSession');

                            return response;
                        });
                })

                .catch(err => {
                    this.set('distributedSession');
                    return Promise.reject(err);
                });
        } catch (err) {
            this.set('distributedSession');
            return Promise.reject(err);
        }
    },

    request(endpoint, method, queryParams, payload, isJsonApiRequest, isEmberStore) {
        let authToken = get(this, 'authToken');
        let isMethodGet = method === 'GET';
        let isMethodDelete = method === 'DELETE';
        let body = isMethodGet || isMethodDelete ? undefined : JSON.stringify(isNone(payload) ? undefined : payload);
        let url = this._buildUrl(endpoint, queryParams);
        let headers = {};

        headers['Accept'] = isJsonApiRequest ? 'application/vnd.api+json' : 'application/json';
        headers['Content-Type'] = isJsonApiRequest ? 'application/vnd.api+json' : 'application/json';

        let sessionId = get(this, 'distributedSession');

        if (isString(sessionId)) {
            headers['Distributed-Session'] = sessionId;
        }

        if (isPresent(authToken)) {
            headers['Authorization'] = `Bearer ${authToken}`;
        }

        return new Promise((resolve, reject) => {
            fetch(url, { method, body, headers }).then(response => {
                if (response.status < 200 || response.status >= 300) {
                    return Promise.reject(response);
                }

                let result = (response.headers.get('content-length') | 0) || (response._bodyBlob && response._bodyBlob.size) ? response.json() : null;

                return Promise.all([response, result]);
            }).then(([response, json]) => {
                let result = null;

                if (isPresent(json) && isJsonApiRequest) {
                    let modelName = this._getModelName(get(json, 'data.type') || 'application');
                    let serializer = this.store.serializerFor(modelName);

                    if (isEmberStore === true) {
                        result = serializer.pushPayload(serializer.store, json);

                        set(result, 'meta', json.meta);
                    } else {
                        result = serializer._createDataset(json);
                    }
                }

                run(null, resolve, [json, result, response]);
            }).catch(error => {
                run(null, reject, error);
            });
        });
    },

    post(url, queryParams, payload, isJsonApiRequest = false, isEmberStore = true) {
        return this.request(url, 'POST', queryParams, payload, isJsonApiRequest, isEmberStore);
    },

    get(url, queryParams, isJsonApiRequest = false, isEmberStore = true) {
        return this.request(url, 'GET', queryParams, undefined, isJsonApiRequest, isEmberStore);
    }
});
