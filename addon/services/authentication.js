import Ember from 'ember';
import _ from 'lodash';

const { Service, get } = Ember;
const { take, isUndefined, isPlainObject } = _;

export default Service.extend({
    authorizationRules: null,

    init() {
        this._super(...arguments);
        this.set('authenticationRules', {});
    },

    getRouteSegments(routeName) {
        let routeSegments = routeName.split('.');

        return routeSegments;
    },

    mustCancelTransistion(routeName) {
        let targetRouteSegments = this.getRouteSegments(routeName);
        let isAuthenticated = this.session.get('isAuthenticated');

        return isAuthenticated && targetRouteSegments[0] === 'login';
    },

    mustAuthenticateRoute(routeName) {
        let authenticationRules = this.get('authenticationRules');
        let targetRouteSegments = this.getRouteSegments(routeName);
        let mustAuthenticate = true;

        for (let i = 0; i < targetRouteSegments.length; i++) {
            let targetRouteName = take(targetRouteSegments, i + 1).join('.');
            let targetRoute = get(authenticationRules, targetRouteName);

            if (isUndefined(targetRoute)) {
                break;
            }

            let isObj = isPlainObject(targetRoute);

            if (isObj) {
                mustAuthenticate = targetRoute.mustAuthenticate !== false;
            } else {
                mustAuthenticate = targetRoute !== false;
            }
        }

        return mustAuthenticate;
    }
});
