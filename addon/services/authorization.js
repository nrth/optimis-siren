import Ember from 'ember';

export default Ember.Service.extend({
    async getRoles() {
        return await this.fetch.get(`authorization/${this.get('applicationId')}/myRoles`);
    },

    async getPermissions() {
        return await this.fetch.get(`authorization/${this.get('applicationId')}/myPermissions`);
    }
});
