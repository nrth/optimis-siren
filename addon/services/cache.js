import Ember from 'ember';
import _ from 'lodash';
import RSVP from 'rsvp';

const { Service, Map } = Ember;
const { cloneDeep, isFunction, isString } = _;
const { Promise } = RSVP;

export default Service.extend({
    init() {
        this._super(...arguments);
        this.set('data', Map.create());
    },

    start(key, fn) {
        if (!isString(key)) {
            throw new Error(`'key' must be a string value`);
        }

        this.get('data').set(key, null);

        return Promise.resolve(fn(() => this._get(key))).then(result => {
            this._delete(key);

            return result;
        });
    },

    update(key, value, postCloneFn) {
        if (!isString(key)) {
            throw new Error(`'key' must be a string value`);
        }

        let data = this.get('data');

        if (!data.has(key)) {
            throw new Error(`cache session was not started for key '${key}'`);
        }

        let clonedValue = cloneDeep(value);

        if (isFunction(postCloneFn)) {
            clonedValue = postCloneFn(clonedValue);
        }

        data.set(key, clonedValue);
    },

    _get(key) {
        if (!isString(key)) {
            throw new Error(`'key' must be a string value`);
        }

        let data = this.get('data');

        if (!data.has(key)) {
            throw new Error(`cache session was not started for key '${key}'`);
        }

        return data.get(key);
    },

    _delete(key) {
        if (!isString(key)) {
            throw new Error(`'key' must be a string value`);
        }

        let data = this.get('data');

        if (!data.has(key)) {
            throw new Error(`cache session was not started for key '${key}'`);
        }

        data.delete(key);
    }
});
