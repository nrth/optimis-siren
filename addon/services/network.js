import Ember from 'ember';
import RSVP from 'rsvp';
import config from '../config';
import promise from 'optimis-siren/utils/promise';
import local from 'optimis-siren/utils/local-storage';
import CordovaInitMixin from 'optimis-siren/mixins/cordova-init-mixin';

const { Service, computed, run, inject } = Ember;
const { later, cancel } = run;
const { service } = inject;
const { not } = computed;
const { Promise } = RSVP;
const isCordova = !!window.cordova;

export default Service.extend(CordovaInitMixin, {
    timer: null,
    isOnline: true,
    isOffline: not('isOnline'),
    fetchService: service('fetch'),
    toastService: service('toast'),

    _cordovaInit() {
        this._super(...arguments);
        this._scheduleOnlineCheck();
    },

    checkForOnline() {
        if (!isCordova) {
            return Promise.resolve(true);
        }

        let fetchService = this.get('fetchService');

        return promise.create(() => fetchService.get('session/ping'), config.networkCheckTimeout).then(() => {
            return this.setOnline(true);
        }).catch(() => {
            return this.setOnline(false);
        });
    },

    _scheduleOnlineCheck() {
        let timer = this.get('timer');

        if (timer) {
            cancel(timer);
        }

        timer = later(this, () => {
            this.checkForOnline().then(() => {
                this._scheduleOnlineCheck();
            });
        }, config.networkCheckInterval);

        this.set('timer', timer);
    },

    setOnline(value) {
        value = !!value;

        if (value !== this.get('isOnline')) {
            let toastService = this.get('toastService');

            if (value) {
                toastService.success('You are now online');
            } else {
                toastService.warning('You have gone offline');
            }
        }

        this.set('isOnline', value);
        local.set('service:network', 'isOnline', value);

        return value;
    }
});
