import Ember from 'ember';

const { Service, run } = Ember;

export default Service.extend({
    toast(message) {
        window.alert(message);
    },

    normalizeMessage(message) {
        return message;
    },

    success(message) {
        run(null, this.toast, this.normalizeMessage(message));
    },

    info(message) {
        run(null, this.toast, this.normalizeMessage(message));
    },

    warning(message) {
        run(null, this.toast, this.normalizeMessage(message));
    },

    error(message) {
        run(null, this.toast, this.normalizeMessage(message));
    }
});
