import Ember from 'ember';
import _ from 'lodash';

const { Helper } = Ember;
const { startsWith } = _;

export default Helper.extend({
	compute([str, target]) {
		return startsWith(str, target);
	}
});
