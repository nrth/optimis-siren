import Ember from 'ember';
import _ from 'lodash';

const { Helper } = Ember;
const { isUndefined } = _;

export default Helper.extend({
	compute([value]) {
		return isUndefined(value);
	}
});
