import Ember from 'ember';
import _ from 'lodash';

const { Helper } = Ember;
const { isNull } = _;

export default Helper.extend({
	compute([value]) {
		return isNull(value);
	}
});
