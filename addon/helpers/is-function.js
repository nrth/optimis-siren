import Ember from 'ember';
import _ from 'lodash';

const { Helper } = Ember;
const { isFunction } = _;

export default Helper.extend({
	compute([value]) {
		return isFunction(value);
	}
});
