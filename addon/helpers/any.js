import Ember from 'ember';

const { Helper } = Ember;

export default Helper.extend({
	compute([array]) {
		return array && array.length;
	}
});
