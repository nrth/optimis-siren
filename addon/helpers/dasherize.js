import Ember from 'ember';

const { Helper } = Ember;
const { dasherize } = Ember.String;

export default Helper.extend({
	compute([str]) {
		return dasherize(str);
	}
});
