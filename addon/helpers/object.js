import Ember from 'ember';

const { Helper, merge } = Ember;

export default Helper.extend({
	compute(args, namedArgs) {
		return merge({}, namedArgs);
	}
});
