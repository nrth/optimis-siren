import Ember from 'ember';

const { Helper, get } = Ember;

export default Helper.extend({
	compute([context, attribute]) {
		let isValid = get(context, `validations.attrs.${attribute}.isValid`);
        let isDirty = get(context, `validations.attrs.${attribute}.isDirty`);

        return isValid === false && isDirty;
	}
});
