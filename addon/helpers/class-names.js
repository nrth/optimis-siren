import Ember from 'ember';
import _ from 'lodash';

const { Helper } = Ember;
const { split, isString } = _;

export default Helper.extend({
	compute([...args]) {
		let classes = [];

		args.forEach(x => {
			if (isString(x)) {
				classes.pushObjects(split(x.replace(/\s+/, ' '), ' '));
			}
		});

		return classes.length > 0 ? classes.join(' ') : undefined;
	}
});
