export default function(application) {
    // authentication
    application.inject('route', 'authentication', 'service:authentication');

    // authorization
    application.inject('route', 'authorization', 'service:authorization');

    // crypto
    application.inject('authenticator', 'crypto', 'service:crypto');

    // fetch
    application.inject('authenticator', 'fetch', 'service:fetch');
    application.inject('service:authorization', 'fetch', 'service:fetch');

    // logger
    application.inject('adapter', 'logger', 'service:logger');
    application.inject('authenticator', 'logger', 'service:logger');
    application.inject('component', 'logger', 'service:logger');
    application.inject('controller', 'logger', 'service:logger');
    application.inject('helper', 'logger', 'service:logger');
    application.inject('route', 'logger', 'service:logger');
    application.inject('serializer', 'logger', 'service:logger');

    // session
    application.inject('component', 'session', 'service:session');
    application.inject('service:fetch', 'session', 'service:session');
    application.inject('service:authentication', 'session', 'service:session');

    // toast
    application.inject('component', 'toast', 'service:toast');
    application.inject('controller', 'toast', 'service:toast');
    application.inject('route', 'toast', 'service:toast');

    // user
    application.inject('component', 'user', 'service:user');
    application.inject('route', 'user', 'service:user');
}
