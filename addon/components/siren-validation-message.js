import Ember from 'ember';
import layout from '../templates/components/siren-validation-message';

const { Component } = Ember;
const SirenValidationMessageComponent = Component.extend({
    layout
});

SirenValidationMessageComponent.reopenClass({
    positionalParams: ['context', 'attribute']
});

export default SirenValidationMessageComponent;
