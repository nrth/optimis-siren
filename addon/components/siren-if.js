import Ember from 'ember';
import layout from '../templates/components/siren-if';
import config from '../config';

const SirenIf = Ember.Component.extend({
    positionalParams: ['predicate'],
    layout,
    tagName: '',
    helperName: 'siren-if'
});

SirenIf.reopenClass({
    positionalParams: ['predicate']
});

export default SirenIf;
