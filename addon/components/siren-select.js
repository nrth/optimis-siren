import Ember from 'ember';
import layout from '../templates/components/siren-select';
import _ from 'lodash';

const { Component, $, guidFor } = Ember;
const { isString, isUndefined, isFunction } = _;

export default Component.extend({
    layout,
    tagName: '',

    init() {
        this._super(...arguments);
        this.set('id', guidFor(this));
    },

    didReceiveAttrs() {
        this._super(...arguments);

        let isMultiple = !!this.getAttr('is-multiple');
        let placeholder = isMultiple ? null : this.getAttr('placeholder');
        let canBeNull = isMultiple ? false : !!this.getAttr('can-be-null');
        let valueMap = this.getAttr('value-map');
        let hasPlaceholder = isString(placeholder);

        this.set('_onChange', this.getAttr('on-change') || (() => { }));
        this.set('_isMultiple', isMultiple);
        this.set('_placeholder', placeholder);
        this.set('hasPlaceholder', hasPlaceholder);
        this.set('canBeNull', canBeNull);
        this.set('valueMap', isFunction(valueMap) ? valueMap : (x => x));

        Promise.resolve(this.getAttr('selected')).then(selected => {
            if (!this.get('isDestroying') && !this.get('isDestroyed')) {
                this.set('_selected', selected);
            }
        });
        
        Promise.resolve(this.getAttr('options')).then(options => {
            if (!this.get('isDestroying') && !this.get('isDestroyed')) {
                this.set('_options', options || []);
            }
        });

        let updateId = this.getAttr('update-id');

        if (isFunction(updateId)) {
            updateId(this.get('id'));
        }
    },

    didRender() {
        this._super(...arguments);

        let $select = this._getElement();

        $select.find('option').each(function() {
            let $this = $(this);

            $this.text($this.text().trim());
        });

        this.setDisabled($select);
    },

    setDisabled($select) {
        let disabled = this.getAttr('disabled');
        let readonly = this.getAttr('readonly');

        if (readonly || disabled) {
            $select.prop('disabled', true);

            if (readonly && !disabled) {
                $select.siblings('.select-dropdown').removeAttr('disabled');
            }
        } else {
            $select.removeAttr('disabled');
        }
    },

    _selectSingle(target) {
        let options = this.get('_options');
        let hasPlaceholder = this.get('hasPlaceholder');
        let canBeNull = this.get('canBeNull');
        let index = target.selectedIndex;

        if (hasPlaceholder) {
            index -= 1;
        }

        if (canBeNull) {
            index -= 1;
        }

        let selectedOption = index < 0 ? null : options.objectAt(index);
        let selectedValue = this.get('valueMap')(selectedOption);

        this.get('_onChange')(selectedValue);
    },

    _selectMultiple(target) {
        let options = this.get('_options');
        let selectedIndices = _(target.options).map((option, i) => !option.selected ? undefined : i).reject(isUndefined).value();
        let selectedOptions = options.objectsAt(selectedIndices);
        let valueMap = this.get('valueMap');
        let selectedValues = selectedOptions.map(valueMap);

        this.get('_onChange')(selectedValues);
    },

    _getElement() {
        let id = this.get('id');
        let $el = $(`#${id}`);

        return $el;
    },

    actions: {
        onChange(e) {
            let isMultiple = this.get('isMultiple');

            if (isMultiple) {
                this._selectMultiple(e.target);
            } else {
                this._selectSingle(e.target);
            }
        }
    }
});
