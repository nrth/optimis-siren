import Ember from 'ember';
import layout from '../templates/components/siren-option';
import AttributeWatchMixin from '../mixins/attribute-watch-mixin';
import _ from 'lodash';

const { isBoolean } = _;

export default Ember.Component.extend(AttributeWatchMixin, {
    layout,
    tagName: 'option',

    _attrs: {
        value: {},
        selected: { isProp: true, use$: true, validate: isBoolean, default: false },
        disabled: { isProp: true, use$: true, validate: isBoolean, default: false },
        hidden: { isProp: true, use$: true, validate: isBoolean, default: false }
    }
});
