import Ember from 'ember';
import layout from '../templates/components/siren-input';
import AttributeWatchMixin from '../mixins/attribute-watch-mixin';
import _ from 'lodash';

const { Component, $, isPresent, run, guidFor } = Ember;
const { isNumber, isFunction, isString } = _;
const { once } = run;

export default Component.extend(AttributeWatchMixin, {
    layout,
    tagName: '',

    _attrs: {
        id: { validate: v => isString(v) && isPresent(v), default: function() { return guidFor(this); } },
        placeholder: { use$: isPresent },
        min: { use$: isNumber },
        max: { use$: isNumber },
        maxLength: { use$: isNumber },
        disabled: { use$: v => !!v, isProp: true },
        readonly: { use$: v => !!v, isProp: true },
        onChange: { validate: isFunction, default: () => () => { } },
        onKeyUp: { validate: isFunction, default: () => () => { } },
        type: { default: 'text' },
        name: { use$: v => isString(v) && isPresent(v) },
        class: { use$: v => isString(v) && isPresent(v) }
    },

    didInsertElement() {
        let $input = this.getElement();
        let onChange = this.get('onChange');
        let onKeyUp = this.get('onKeyUp');

        $input.bind('propertychange change input paste', () => {
            let value = $input.val();
            let type = this.get('type');

            if (type === 'number') {
                let num = parseFloat(value);

                if (!isNaN(num)) {
                    once(this, onChange, num);
                    return;
                }
            }

            once(this, onChange, value);
        });

        $input.bind('keyup', e => {
            once(this, onKeyUp, e);
        });

        $input.bind('keydown', e => {
            let value = $input.val();
            let type = this.get('type');

            if (type === 'number') {
                let regex = /\./;
                let selectedText = window.getSelection().toString();
                
                if (e.keyCode === 190 && regex.test(value) && !regex.test(selectedText)) {
                    e.preventDefault();
                    return;
                }

                if ($.inArray(e.keyCode, [8, 9, 13, 16, 27, 46, 110, 190]) !== -1 ||
                    // Allow: Ctrl/cmd+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl/cmd+C
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl/cmd+X
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }

                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                    return;
                }
            }
        });

        $input.bind('blur', e => {
            let value = $input.val();
            let type = this.get('type');

            if (type === 'number') {
                let min = parseFloat(this.get('min'));
                let max = parseFloat(this.get('max'));

                value = parseFloat(value) || 0;

                if (isNumber(min) && value < min) {
                    value = min;
                } else if (isNumber(max) && value > max) {
                    value = max;
                }

                $input.val(value);
            }

            once(this, onChange, value);
        })
    },

    getElement() {
        let id = this.get('id');
        let $input = $(`#${id}`);

        if ($input.length > 1) {
            throw new Error(`Expected only one element with id '${id}', found ${$input.length}`);
        }

        return $input;
    }
});
