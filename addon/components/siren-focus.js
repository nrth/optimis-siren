import Ember from 'ember';
import layout from '../templates/components/siren-focus';

const { Component, $ } = Ember;

export default Component.extend({
    layout,
    tagName: '',

    init() {
        this._super(...arguments);
        this.set('id', this.uuid.generate());
    },

    didRender() {
        let $control = $(`#${this.get('id')}`);

        $control.focus();
        this.set('$control', $control);
    }
});
