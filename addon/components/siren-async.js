import Ember from 'ember';
import layout from '../templates/components/siren-async';
import AttributeWatchMixin from 'optimis-siren/mixins/attribute-watch-mixin';
import _ from 'lodash';
import RSVP from 'rsvp';

const { Component, run } = Ember;
const { isFunction } = _;
const { Promise } = RSVP;
const SirenAsyncComponent = Component.extend(AttributeWatchMixin, {
    layout,
    tagName: '',
    isWorking: false,

    _attrs: {
        asyncAction: { validate: isFunction, default: () => () => {} },
        setAsync: { validate: isFunction, default: () => () => {} }
    },

    actions: {
        performAsyncAction() {
            let action = this.get('asyncAction');
            let setIsWorking = isWorking => {
                if (!this.get('isDestroyed')) {
                    this.set('isWorking', isWorking);
                }

                this.get('setAsync')(isWorking);
            };

            setIsWorking(true);

            try {
                return Promise.resolve(action(...arguments)).then(() => {
                    run(() => {
                        setIsWorking(false);
                    });
                }).catch(() => {
                    run(() => {
                        setIsWorking(false);
                    });
                });
            } catch (error) {
                this.logger.error(error);
                setIsWorking(false);
            }
        }
    }
});

SirenAsyncComponent.reopenClass({
    positionalParams: ['async-action', 'set-async']
});

export default SirenAsyncComponent;
