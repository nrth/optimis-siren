import Ember from 'ember';
import layout from '../templates/components/siren-let';

const { Component } = Ember;
const SirenLetComponent = Component.extend({
    layout,
    tagName: ''
});

SirenLetComponent.reopenClass({ positionalParams: ['param1', 'param2', 'param3', 'param4', 'param5', 'param6', 'param7', 'param8'] });

export default SirenLetComponent;
