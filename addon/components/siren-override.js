import Ember from 'ember';

const { Component } = Ember;

export default Component.extend({
    tagName: '',

    didReceiveAttrs() {
        let actual = this.getAttr('params1');
        let check = this.getAttr('params2');

        this.set('actual', actual);
        this.set('check', check);
    },
});
