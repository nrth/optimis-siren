import Ember from 'ember';

const EmberError = Ember.Error;

export function OfflineError() {
	EmberError.call(this, 'The application is offline.');
}

OfflineError.prototype = Object.create(EmberError.prototype);

export default { OfflineError };
