export default function(applicationInstance, ENV, authenticationRules, authorizationRules) {
    // authentication
    let authentication = applicationInstance.lookup('service:authentication')

    authentication.set('authenticationRules', authenticationRules);

    // authorization
    let authorization = applicationInstance.lookup('service:authorization')

    authorization.set('authorizationRules', authorizationRules);
    authorization.set('applicationId', ENV.authorization.applicationId);

    // network
    applicationInstance.lookup('service:network');
}
