import benchmark from 'optimis-siren/utils/benchmark';

export default function(applicationInstance) {
    benchmark.logger = applicationInstance.lookup('service:logger');
}
