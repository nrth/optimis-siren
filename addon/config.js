import Ember from 'ember';
import _ from 'lodash';

const { get } = Ember;
const { isPlainObject } = _;
const Defaults = {
    host: undefined,
    rootUrl: undefined,
    apiNamespace: 'api',
    logLevel: 'debug',
    isGuidByteArray: false,
    networkCheckInterval: 10000,
    networkCheckTimeout: 5000,
    authentication: {},
    authorization: {}
};

export default {
    host: Defaults.host,
    rootUrl: Defaults.rootUrl,
    apiNamespace: Defaults.apiNamespace,
    logLevel: Defaults.logLevel,
    isGuidByteArray: Defaults.isGuidByteArray,
    networkCheckInterval: Defaults.networkCheckInterval,
    networkCheckTimeout: Defaults.networkCheckTimeout,
    authentication: Defaults.authentication,
    authorization: Defaults.authorization,

    load(env, authentication, authorization) {
        // APP
        this.host = get(env, 'APP.host') || Defaults.host;
        this.rootUrl = get(env, 'rootURL') || Defaults.rootUrl;
        this.apiNamespace = get(env, 'APP.apiNamespace') || Defaults.apiNamespace;
        // logger
        this.logLevel = get(env, 'logger.level') || Defaults.logLevel;

        this.isGuidByteArray = get(env, 'APP.isGuidByteArray') || Defaults.isGuidByteArray;

        // Network

        this.networkCheckInterval = get(env, 'services.network.interval') || Defaults.networkCheckInterval;
        this.networkCheckTimeout = get(env, 'services.network.timeout') || Defaults.networkCheckTimeout;

        // Authentication

        this.authentication = isPlainObject(authentication) ? authentication : Defaults.authentication;

        // Authorization

        this.authorization = isPlainObject(authorization) ? authorization : Defaults.authorization;
    }
};
