import Ember from 'ember';
import Url from 'optimis-siren/utils/url';

const { Mixin } = Ember;

export default Mixin.create({
    _buildUrl(endpoint, queryParams) {
        return Url.build(endpoint, queryParams);
    }
});
