import Ember from 'ember';
import _ from 'lodash';

const {
    $,
    Mixin,
    isPresent,
    String: { camelize, dasherize },
    isArray,
    isNone,
    merge
} = Ember;

const { isUndefined, isFunction, isBoolean, isNumber, isString, isPlainObject } = _;

function assertValueType(value, fn, message, canBeNull = false) {
    if (isNone(value) && !canBeNull) {
        throw new TypeError(`'value' must not be null or undefined`);
    }

    if (!fn(value)) {
        throw new TypeError(message);
    }
}

export function validate(_default, normalizeFunc) {
    let result = {};

    if (!isNone(_default)) {
        result.default = _default;
    }

    if (isFunction(normalizeFunc)) {
        result.normalize = normalizeFunc;
    }

    return result;
}

export function validateFunction(_default = function() {}, normalizeFunc) {
    assertValueType(_default, isFunction, 'Default value should be a function');

    let _default2 = function() {
        return _default;
    };

    let result = { validate: isFunction, default: _default2 };
    
    if (isFunction(normalizeFunc)) {
        result.normalize = normalizeFunc;
    }

    return result;
}

export function validateBoolean(_default = false, normalizeFunc) {
    assertValueType(_default, isBoolean, 'Default value should be a boolean');

    let result = { validate: isBoolean, default: _default };
    
    if (isFunction(normalizeFunc)) {
        result.normalize = normalizeFunc;
    }

    return result;
}

export function validateString(_default = '', normalizeFunc) {
    assertValueType(_default, isString, 'Default value should be a string');

    let result = { validate: isString, default: _default };
    
    if (isFunction(normalizeFunc)) {
        result.normalize = normalizeFunc;
    }

    return result;
}

export function validateNumber(_default = 0, normalizeFunc) {
    assertValueType(_default, isNumber, 'Default value should be a number');

    let result = { validate: isNumber, default: _default };
    
    if (isFunction(normalizeFunc)) {
        result.normalize = normalizeFunc;
    }

    return result;
}

export function validateArray(_default = [], normalizeFunc) {
    assertValueType(_default, isArray, `'_default' should be an array`);

    let result = { validate: isArray, default: _default };
    
    if (isFunction(normalizeFunc)) {
        result.normalize = normalizeFunc;
    }

    return result;
}

export function validatePlainObject(_default = {}, normalizeFunc) {
    assertValueType(_default, isPlainObject, `'_default' should be a plain object`);

    let result = { validate: isPlainObject, default: _default };
    
    if (isFunction(normalizeFunc)) {
        result.normalize = normalizeFunc;
    }

    return result;
}

export default Mixin.create({
    _attrName: '_attrs',
    _resolvePromises: true,
    _watchNameIn: dasherize,
    _watchNameOut: dasherize,
    _watchValueName: camelize,

    initAttrs() {
        return {};
    },

    getElement() {
        if (isPresent(this.get('tagName'))) {
            return this.$();
        }

        let id = this.get('id');

        if (isPresent(id)) {
            return $(`#${id}`);
        }        
    },

    _getAttrs() {
        let attrs = this.initAttrs();
        let name = this.get('_attrName');
        let _attrs = this.get(name);

        return merge(attrs, _attrs);
    },

    didReceiveAttrs() {
        this._super(...arguments);

        let resolvePromises = this.get('_resolvePromises');
        let attrs = this._getAttrs();
        let nameIn = this.get('_watchNameIn');
        let valueName = this.get('_watchValueName');
        let setValue = (attr, key, value) => {
            let hasValidateFunction = isFunction(attr.validate);            

            if (hasValidateFunction && !attr.validate(value) || !hasValidateFunction && !value && !!attr.default) {
                value = isFunction(attr.default) ? attr.default.call(this, value) : attr.default;
            }

            if (isFunction(attr.normalize)) {
                value = attr.normalize.call(this, value);
            }

            this.set(valueName(key), value);
        };

        for (let key in attrs) {
            let attr = attrs[key];
            let attrValue = this.get(nameIn(key));
            let resolve = resolvePromises || attr.resolvePromise;

            if (resolve && !!attrValue && isFunction(attrValue.then)) {
                attrValue.then(v => {
                    setValue(attr, key, v);
                });
            } else {
                setValue(attr, key, attrValue);
            }
        }
    },

    didRender() {
        this._super(...arguments);

        let $element = this.getElement();

        if (isUndefined($element)) {
            return;
        }

        let attrs = this._getAttrs();
        let nameOut = this.get('_watchNameOut');
        let valueName = this.get('_watchValueName');

        for (let key in attrs) {
            let attr = attrs[key];

            if (!attr.use$) {
                continue;
            }

            if (attr.isProp) {
                this._setProp($element, nameOut(key), this.get(valueName(key)), attr.use$);
            } else {
                this._setAttr($element, nameOut(key), this.get(valueName(key)), attr.use$);
            }
        }
    },

    _setAttr($element, name, value, use$) {
        if (!isFunction(use$) || use$.call(this, value)) {
            $element.attr(name, value);
        } else {
            $element.removeAttr(name);
        }
    },

    _setProp($element, name, value, use$) {
        if (!isFunction(use$) || use$.call(this, value)) {
            $element.prop(name, value);
        } else {
            $element.removeAttr(name);
        }
    }
});
