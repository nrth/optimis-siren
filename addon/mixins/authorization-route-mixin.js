import Ember from 'ember';
import RSVP from 'rsvp';
import _ from 'lodash';
import config from 'optimis-siren/config';

const { Mixin, isArray, get } = Ember;
const { Promise } = RSVP;
const { take, isPlainObject, intersection } = _;
const { authorization } = config;

export default Mixin.create({
    unauthorizedRouteName: 'unauthorized',

    async beforeModel(transition) {
        await Promise.resolve(this._super(...arguments));

        let targetRouteSegments = this.getRouteSegments(transition);

        this.authorize(transition, targetRouteSegments);
    },

    getRouteSegments(transition) {
        let routeSegments = transition.targetName.split('.');

        return routeSegments;
    },

    async authorize(transition, targetRouteSegments) {
        let unauthorizedRouteName = this.get('unauthorizedRouteName');

        for (let i = 0; i < targetRouteSegments.length; i++) {
            let targetRouteName = take(targetRouteSegments, i + 1).join('.');
            let targetRoute = get(authorization, targetRouteName);
            let isObj = isPlainObject(targetRoute);
            let isArr = isArray(targetRoute);

            if (isObj || isArr) {
                let mustBlockCordova = isObj && targetRoute.mustBlockCordova === true;

                if (mustBlockCordova) {
                    this.logger.warn(`Cordova can't access route '${targetRouteName}'`);
                    this.transitionTo(unauthorizedRouteName);

                    return;
                }

                let requiredPermissions = isObj && isArray(targetRoute.requiredPermissions) ? targetRoute.requiredPermissions : targetRoute;

                if (requiredPermissions.length > 0) {
                    let permissions = await this.authorization.getPermissions();
                    let overlappingPermissions = intersection(requiredPermissions, permissions);

                    if (overlappingPermissions.length === 0) {
                        this.toast.error(`You do not have the required permissions to access this page`);
                        this.logger.warn(`User does not have the appropriate permissions to access route '${targetRouteName}'`);
                        this.transitionTo(unauthorizedRouteName);

                        return;
                    }
                }
            } else {
                this.logger.debug(`Route '${targetRouteName}' does not require any permissions`);
            }
        }
    }
});
