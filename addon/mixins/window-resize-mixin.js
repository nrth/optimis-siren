import Ember from 'ember';
import _ from 'lodash';

const { Mixin, run } = Ember;
const { debounce } = run;
const { isFunction } = _;

export default Mixin.create({
    init() {
        this._super(...arguments);

        if (!isFunction(this.didWindowResize)) {
            return;
        }

        let placeholder = 'hello';
        let eventListener = () => {
            if (this.get('isDestroyed')) {
                window.removeEventListener('resize', placeholder, false);
            } else {
                debounce(this, resize, this.get('windowResizeWait') | 0);
            }
        }

        placeholder = eventListener;

        function resize() {
            this.didWindowResize();
        }

        window.addEventListener('resize', eventListener, false);
    }
});
