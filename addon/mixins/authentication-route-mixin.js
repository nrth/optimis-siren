import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

const { Mixin } = Ember;

export default Mixin.create(AuthenticatedRouteMixin, {
    beforeModel(transition) {
        let mustCancelTransistion = this.authentication.mustCancelTransistion(transition.targetName);

        if (mustCancelTransistion) {
            this.transitionTo('index');
            return;
        }

        let mustAuthenticate = this.authentication.mustAuthenticateRoute(transition.targetName);

        if (mustAuthenticate === true) {
            return this._super(...arguments);
        }
    }
});
