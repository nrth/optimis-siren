import Ember from 'ember';

const {
    Mixin,
    String: {
        camelize
    }
} = Ember;

export default Mixin.create({
	_getModelName(name) {
		return camelize(name);
	},
	
	_getPayloadModelName(name) {
		return camelize(name);
	},
});
