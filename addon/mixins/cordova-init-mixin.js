import Ember from 'ember';
import _ from 'lodash';

const { Mixin, get, set } = Ember;
const { isFunction } = _;
const isCordova = !!window.cordova;

export default Mixin.create({
    _initFnName: '_init',
    _initCordovaFnName: '_cordovaInit',

    init() {
        this._super(...arguments);

        let initFnName = get(this, '_initFnName');
        let initCordovaFnName = get(this, '_initCordovaFnName');
        let initFn = get(this, initFnName);
        let initCordovaFn = get(this, initCordovaFnName);

        if (!isFunction(initFn)) {
            set(this, initFnName, () => { });
        }

        if (!isFunction(initCordovaFn)) {
            set(this, initCordovaFnName, () => { });
        }

        if (isCordova) {
            document.addEventListener('deviceready', () => {
                initCordovaFn = get(this, initCordovaFnName);

                initCordovaFn.apply(this, arguments);
            }, false);
        } else {
            initFn = get(this, initFnName);

            initFn.apply(this, arguments);
        }
    }
});
