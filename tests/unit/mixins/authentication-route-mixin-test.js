import Ember from 'ember';
import AuthenticationRouteMixinMixin from 'optimis-siren/mixins/authentication-route-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | authentication route mixin');

// Replace this with your real tests.
test('it works', function(assert) {
  let AuthenticationRouteMixinObject = Ember.Object.extend(AuthenticationRouteMixinMixin);
  let subject = AuthenticationRouteMixinObject.create();
  assert.ok(subject);
});
