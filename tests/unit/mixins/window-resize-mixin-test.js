import Ember from 'ember';
import WindowResizeMixinMixin from 'optimis-siren/mixins/window-resize-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | window resize mixin');

// Replace this with your real tests.
test('it works', function(assert) {
  let WindowResizeMixinObject = Ember.Object.extend(WindowResizeMixinMixin);
  let subject = WindowResizeMixinObject.create();
  assert.ok(subject);
});
