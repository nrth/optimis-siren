(function() {
  function vendorModule() {
    'use strict';

    return {
      'default': self['enquire'],
      __esModule: true,
    };
  }

  define('enquire', [], vendorModule);
})();
